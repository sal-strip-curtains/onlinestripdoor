<?php

namespace App\Http\Controllers;

use App\Models\StripDoorHardware;
use Illuminate\Http\Request;

class StripDoorHardwareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StripDoorHardware  $stripDoorHardware
     * @return \Illuminate\Http\Response
     */
    public function show(StripDoorHardware $stripDoorHardware)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StripDoorHardware  $stripDoorHardware
     * @return \Illuminate\Http\Response
     */
    public function edit(StripDoorHardware $stripDoorHardware)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StripDoorHardware  $stripDoorHardware
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StripDoorHardware $stripDoorHardware)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StripDoorHardware  $stripDoorHardware
     * @return \Illuminate\Http\Response
     */
    public function destroy(StripDoorHardware $stripDoorHardware)
    {
        //
    }
}
