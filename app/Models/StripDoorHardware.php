<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StripDoorHardware extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','sku','description','door_height','unit','weight','price','image','status','order'
    ];
}
